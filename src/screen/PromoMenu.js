import React from "react";
import { Image } from "react-native";
import {
    Container,
    Content,
    Button,
    Thumbnail,
    Card,
    CardItem
} from "native-base";
import { AppHeader, AppFooter } from "../app-nav/index"

export default class PromoMenu extends React.Component {
  render() {
    return (
      <Container>
       <AppHeader isMenu navigation={this.props.navigation} title="Telkom Corner" />
       <Content>
         <Button style={{ height: 100, width: '100%', alignSelf: 'center' }} transparent onPress={() => this.props.navigation.navigate("Promo")}>
           <Card transparent>
             <CardItem cardBody>
               <Image style={{ width: '100%' }} source={require("../../img/asset/promo/group_4.png")} />
             </CardItem>
           </Card>
         </Button>
               <Button style={{ height: 100, width: '100%', alignSelf: 'center' }} transparent onPress={() => this.props.navigation.navigate("PetaGrapari")}>
           <Card transparent>
             <CardItem cardBody>
                 <Image style={{ width: '100%' }} source={require("../../img/asset/promo/group_3.png")} />
             </CardItem>
           </Card>
         </Button>
               <Button style={{ height: 100, width: '100%', alignSelf: 'center' }} transparent onPress={() => this.props.navigation.navigate("Donasi")}>
           <Card transparent>
             <CardItem cardBody>
                 <Image style={{ width: '100%' }} source={require("../../img/asset/promo/group_2.png")} />
             </CardItem>
           </Card>
         </Button>
       </Content>
     </Container>
    );
  }
    
  go(param) {
    if (param == "promo") this.props.navigation.navigate("Promo");
  }
};